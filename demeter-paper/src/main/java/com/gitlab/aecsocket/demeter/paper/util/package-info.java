/**
 * Various plugin utilities.
 */
@javax.annotation.ParametersAreNonnullByDefault
package com.gitlab.aecsocket.demeter.paper.util;
